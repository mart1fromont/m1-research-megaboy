# Get the sprite's name
def get_sprite_name(sprite):
    """
    Get the sprite's name

    Parameters:
    ----------
    sprite: int
        The sprite number

    Returns:
    -------
    str
        The sprite's name
    """

    if sprite == Constants.sprite_player_bullet:
        return 'player_bullet'
    elif sprite in Constants.sprites_power_item:
        return 'power_item'
    elif sprite == Constants.sprite_weapon_refill_item:
        return 'weapon_refill_item'
    elif sprite in Constants.sprites_flying_enemy_tiles:
        return 'enemy_1'
    elif sprite in Constants.sprites_enemy_2_tiles:
        return 'enemy_2'
    #elif sprite in Constants.sprites_box:
    #    return 'box'
    else:
        return "unknown"


class Constants:
    """
    Constants class
    This class contains all the constants used by the emulator
    """

    # Player position
    player_position_y_address = 0xFE00  # 65024
    player_position_x_address = 0xFE01  # 65025

    # Screen scroll
    scroll_x_address = 0xFF43  # 65251
    scroll_y_address = 0xFF42  # 65250

    # Lives
    player_lives_count = 0xDF34
    player_current_life = 0xDE9E

    # Items
    sprite_player_bullet = 0x0D
    sprites_power_item = [0x8A, 0x8B]
    sprite_weapon_refill_item = 0x88

    # Ennemies
    # Flying enemy 1
    sprites_flying_enemy_tiles = [0x95, 0x96, 0x97, 0x98, 0x90, 0x8F, 0x8D, 0x8E]
    # sprites_flying_enemy_bullet = 0x15

    # Enemy 2
    sprites_enemy_2_tiles = [0x8D, 0x8E, 0x8F, 0x92, 0x91, 0x90, 0x96, 0x97, 0x98, 0x9D, 0x9F, 0xA3, 0xA5, 0xA6, 0x95]
    # sprites_enemy_2_bullet = [0xA9, 0xAA, 0xAB, 0xAC, 0xAD]

    # Box
    sprites_box = [0x9B, 0x9C, 0x9D, 0x9E]
