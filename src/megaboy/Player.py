import datetime
from abc import ABC

from pyboy import WindowEvent

from src.megaboy.Constants import Constants, get_sprite_name
from src.utils.Position import Position


class Player(ABC):
    """
    Player class
    Used to interact with the player on the emulator

    Attributes:
    ----------
    pyboy: pyboy.PyBoy
        PyBoy emulator instance. Required to interact with the emulator
    logger: Logger
        The Logger instance. Required to use the logger
    manager: PyBoyManager
        The PyBoyManager instance. Required to interact with the sprites

    position: Tilemap.Position
        The current position of the player. Position is relative to the Game Boy screen, not the game area
    scroll: Tilemap.Position
        The current scroll of the game area. Scroll is relative
    life: int
        The current life of the player. 0 means dead, and max is 152 (default 152)
    lives_left: int
        The number of lives left for the player. Normally between 0 and 3 (default 3)
    """

    def __init__(self, _pyboy, _manager, _logger):
        """
        Initialize the Player

        Parameters:
        ----------
        _pyboy: pyboy.PyBoy
            PyBoy emulator instance. Required to interact with the emulator
        _manager: PyBoyManager
            The PyBoyManager instance. Required to interact with the sprites
        _logger: Logger
            The Logger instance. Required to use the logger
        """

        self.pyboy = _pyboy
        self.manager = _manager
        self.logger = _logger
        self.position = Position
        self.scroll = Position
        self.alive = True
        self.lives_left = 3
        self.life = 152
        self.start_time = datetime.datetime.now()
        self.update()

    def update(self):
        """
        Update all the player information
        Run on each emulator frame
        """

        self.position = Position(self.pyboy.get_memory_value(Constants.player_position_x_address),
                                 self.pyboy.get_memory_value(Constants.player_position_y_address))
        self.scroll = Position(self.pyboy.get_memory_value(Constants.scroll_x_address),
                               self.pyboy.get_memory_value(Constants.scroll_y_address))

        self.life = self.pyboy.get_memory_value(Constants.player_current_life)
        self.lives_left = self.pyboy.get_memory_value(Constants.player_lives_count)
        self.pyboy.set_memory_value(Constants.player_lives_count, 5)

    def is_alive(self):
        """
        Check if the player is alive

        Returns:
        -------
        bool
            True if the player is alive, False otherwise
        """

        self.update()
        return self.life > 0

    def get_ennemies(self) -> list:
        """
        Get the list of ennemies on the screen

        Parameters:
        ----------
        to_list: bool
            If True, return a list of ennemies, otherwise return a list of ennemies' names

        Returns:
        -------
        list
            List of ennemies
        """

        ennemies = []
        names = []

        for i in range(40):
            sprite = self.manager.sprite(i)

            # Check if sprite is from an enemy
            enemy = get_sprite_name(sprite.tile_identifier)

            # If the enemy's sprite is not already on screen, add it
            if not enemy in names and sprite.on_screen and enemy.startswith('enemy'):
                # Check if enemy is left or right of the player
                '''if sprite.x < self.position.x:
                    enemy += "_left"
                else:
                    enemy += "_right"

                # Check if enemy is above or below the player
                if sprite.y < self.position.y:
                    enemy += "_up"
                else:
                    enemy += "_down" '''

                names.append(enemy)
                ennemies.append({
                    # 'position': f"{sprite.x // 5},{sprite.y // 5}",
                    'name': enemy
                })

        return ennemies

    def time(self):
        """
        Get the current time

        Returns:
        -------
        int
            The current time in seconds
        """

        return (datetime.datetime.now() - self.start_time).seconds

    def has_wall_ahead(self):
        """
        Check if there is a wall ahead of the player

        Returns:
        -------
        bool
            True if there is a wall ahead, False otherwise
        """
        return False

    def move_left(self):
        """
        Move the player to the left
        """

        self.pyboy.send_input(WindowEvent.PRESS_ARROW_LEFT)
        for _ in range(2):
            self.pyboy.tick()
        self.pyboy.send_input(WindowEvent.RELEASE_ARROW_LEFT)
        self.pyboy.tick()
        self.logger.debug("Player moved left to {}".format(self.position))

    def move_right(self):
        """
        Move the player to the right
        """

        self.pyboy.send_input(WindowEvent.PRESS_ARROW_RIGHT)
        for _ in range(2):
            self.pyboy.tick()
        self.pyboy.send_input(WindowEvent.RELEASE_ARROW_RIGHT)
        self.pyboy.tick()
        self.logger.debug("Player moved right to {}".format(self.position))

    def move_up(self):
        """
        Move the player up
        """

        self.pyboy.send_input(WindowEvent.PRESS_ARROW_UP)
        for _ in range(2):
            self.pyboy.tick()
        self.pyboy.send_input(WindowEvent.RELEASE_ARROW_UP)
        self.pyboy.tick()
        self.logger.debug("Player moved up to {}".format(self.position))

    def move_down(self):
        """
        Move the player down
        """

        self.pyboy.send_input(WindowEvent.PRESS_ARROW_DOWN)
        for _ in range(2):
            self.pyboy.tick()
        self.pyboy.send_input(WindowEvent.RELEASE_ARROW_DOWN)
        self.pyboy.tick()
        self.logger.debug("Player moved down to {}".format(self.position))

    # fixme: deprecated
    def jump(self):
        """
        Jump straight up
        """

        self.pyboy.send_input(WindowEvent.PRESS_BUTTON_A)
        for _ in range(20):
            self.pyboy.tick()
        self.pyboy.send_input(WindowEvent.RELEASE_BUTTON_A)
        for _ in range(5):
            self.pyboy.tick()
        self.logger.debug("Player jumped straight to {}".format(self.position))

    # fixme: deprecated
    def jump_left(self):
        """
        Jump to the left
        """

        self.pyboy.send_input(WindowEvent.PRESS_ARROW_LEFT)
        self.pyboy.send_input(WindowEvent.PRESS_BUTTON_A)
        for _ in range(20):
            self.pyboy.tick()
        self.pyboy.send_input(WindowEvent.RELEASE_BUTTON_A)
        self.pyboy.send_input(WindowEvent.RELEASE_ARROW_LEFT)
        for _ in range(5):
            self.pyboy.tick()
        self.logger.debug("Player jumped left to {}".format(self.position))

    # fixme: deprecated
    def jump_right(self):
        """
        Jump to the right
        """

        self.pyboy.send_input(WindowEvent.PRESS_ARROW_RIGHT)
        self.pyboy.send_input(WindowEvent.PRESS_BUTTON_A)
        for _ in range(20):
            self.pyboy.tick()
        self.pyboy.send_input(WindowEvent.RELEASE_BUTTON_A)
        self.pyboy.send_input(WindowEvent.RELEASE_ARROW_RIGHT)
        for _ in range(5):
            self.pyboy.tick()
        self.logger.debug("Player jumped right to {}".format(self.position))

    def shoot(self):
        """
        Shoot a bullet
        """

        self.pyboy.send_input(WindowEvent.PRESS_BUTTON_B)
        self.pyboy.tick()
        self.pyboy.send_input(WindowEvent.RELEASE_BUTTON_B)
        self.pyboy.tick()
        self.logger.debug("Player shot a bullet from {}".format(self.position))

    def dash(self):
        """
        Make the player dash
        """

        self.pyboy.send_input(WindowEvent.PRESS_ARROW_DOWN)
        self.pyboy.send_input(WindowEvent.PRESS_BUTTON_A)
        self.pyboy.tick()
        self.pyboy.send_input(WindowEvent.RELEASE_BUTTON_A)
        self.pyboy.send_input(WindowEvent.RELEASE_ARROW_DOWN)
        self.pyboy.tick()
        self.logger.debug("Player dashed to {}".format(self.position))

    def jump_start(self):
        """
        Start a jump
        """

        self.pyboy.send_input(WindowEvent.PRESS_BUTTON_A)
        self.pyboy.tick()
        self.logger.debug("Player started a jump from {}".format(self.position))

    def jump_end(self):
        """
        End a jump
        """

        self.pyboy.send_input(WindowEvent.RELEASE_BUTTON_A)
        self.pyboy.tick()
        self.logger.debug("Player ended a jump to {}".format(self.position))

    def stay(self):
        """
        Stay in place
        """

        self.pyboy.tick()
        self.logger.debug("Player stayed in {}".format(self.position))
