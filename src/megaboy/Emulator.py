import os

import keyboard
import numpy as np
from numba import cuda

from pyboy import pyboy

from src.megaboy.Player import Player
from src.megaboy.models.Model_Wrapper import ModelType
from src.megaboy.models.NEAT.NEAT_Model import NEATModel
from src.megaboy.models.q_learning.Q_Learning_Model import QLearningModel
from src.utils.Logger import Logger

default_rom_path = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "tools", "megaman.gb")


class Emulator:
    """
    Emulator class
    Manages an instance of a PyBoy emulator

    Attributes
    ----------
    rom : str
        The path to the ROM file
    pyboy : pyboy.PyBoy
        The PyBoy instance
    manager : pyboy.BotSupportManager
        PyBoy bot support manager instance of the emulator
    logger : Logger
        The logger instance to use
    player : Player
        The Player instance to interact with the player on the emulator
    model : ModelType
        The model instance to interact with the emulator (default is Q-learning)
    """

    def __init__(self,
                 rom_file_path=default_rom_path,
                 load_state=None,
                 logger=None,
                 model=ModelType.Q_LEARNING,
                 start_q_table=None,
                 start_winner=None):
        """
        Initializes the emulator

        Parameters
        ----------
        rom_file_path : str
            The path to the ROM file. Uses the default ROM file if not provided
        load_state : BinaryIO
            The path to the save state file. Loads the emulator from scratch if not provided
        logger : Logger
            The logger instance to use. Required for utils
        model : ModelType
            The model instance to interact with the emulator (default is Q-learning)
        start_q_table : str
            The path to a pre-existing Q-table file. Used when model is Q-learning. Starts with a new Q-table if not provided
        start_winner : str
            The path to a pre-existing winner file. Used when model is NEAT. Starts with a new winner if not provided
        """

        # Stop if no logger is provided
        if logger is None:
            self.logger = Logger()
            self.logger.warning('No logger provided. Will use a new instance of Logger')
        else:
            self.logger = logger

        # If no rom file path is provided or the file does not exist, use the default rom file
        if not rom_file_path or len(rom_file_path) == 0 or not os.path.isfile(rom_file_path):
            self.logger.warning('No ROM file path provided or the file does not exist. Using the default ROM file')
            rom_file_path = default_rom_path

        self.rom = rom_file_path
        self.pyboy = pyboy.PyBoy(self.rom, cgb=True, sound=False, scale=2, debug=False)
        self.pyboy.set_emulation_speed(0)
        self.manager = self.pyboy.botsupport_manager()
        logger.info('Emulator initialized. ROM file is: {}'.format(self.rom))

        # Initialize the player
        self.player = Player(self.pyboy, self.manager, self.logger)
        logger.info('Player initialized')

        # Initialize the model
        if model == ModelType.Q_LEARNING:
            self.logger.info('Initializing emulator with Q-learning model')
            if start_q_table is None:
                self.logger.info('No Q-table provided. Starting with a new Q-table')
                self.model = QLearningModel(player=self.player, logger=self.logger)
            else:
                self.logger.info('Loading Q-table from file: {}'.format(start_q_table))
                load_q_table = np.load(start_q_table)
                self.model = QLearningModel(player=self.player, logger=self.logger, start_table=load_q_table)

        elif model == ModelType.NEAT:
            self.logger.info('Initializing emulator with NEAT model')

            if start_winner is not None:
                self.logger.info('Loading winner from file: {}'.format(start_winner))

            self.model = NEATModel(player=self.player, logger=self.logger, start_winner=start_winner)

        # When S key is pressed, the current Q-table is saved
        keyboard.on_press_key("m", lambda _: self.create_save_state())

        # Load a save state if provided
        if load_state is not None:
            logger.info('Loading save state: {}'.format(load_state))
            self.pyboy.load_state(load_state)

        self.run()

    def create_save_state(self):
        """
        Creates a save state of the emulator
        """

        self.logger.info('Creating save state')
        self.pyboy.save_state('emulator.state')

    def run(self):
        """
        Emulator loop
        This will run the emulator frame by frame at the emulator's speed
        """

        self.logger.info('Emulator will play {}'.format(self.pyboy.cartridge_title()))
        self.logger.info('Starting emulator player loop')

        # if the system has a GPU, use numba.cuda.jit to compile the function
        if cuda.is_available():
            self.logger.info(f"A GPU has been found ({cuda.get_current_device()}), using CUDA to accelerate training")
            self.model.train_gpu()

        else:
            self.logger.warning(
                "Beware ! No GPU has been detected on your system, training might be slower than expected")
            self.model.train()

        # Close the emulator when pyboy instance is over
        #self.logger.info('Emulator closed')
        #self.pyboy.stop()

    def memory_test(self, complexity):
        """
        Memory test

        Tests for game memory in the emulator

        Parameters
        ----------
        complexity : int
            The complexity of the test. 0 is no logs and 1 you get all logs
        """

        if complexity > 0:
            print("sprites in screen :")
            for i in range(40):

                # MF: this one removes duplicates
                sprite = self.manager.sprite(i)

                '''
                MF: when we'll have the sprite's list, we'll be able to do this:
                       for e.g., assume enemy A is sprite at index i:
                       enemies_a :: List = [x for x in self.manager.sprite_by_tile_identifier(i) if x.on_screen]  => gets all enemies A on screen
                '''

                if sprite.on_screen:
                    print(sprite)

                if sprite.tile_identifier > 51:
                    print(f"enemy at ({sprite.x}, {sprite.y}) | code is {sprite.tile_identifier}")