import os
import pickle
import re
from abc import ABC

import neat
import numpy as np
from neat.math_util import softmax

from src.megaboy.models.Model_Wrapper import ModelWrapper, ModelType
from src.megaboy.models.NEAT import visualize


# NEAT model class to set up utilising the neat-python library
# verifies that the NEAT library is installed on the system
class NEATModel(ModelWrapper, ABC):

    def __init__(self, player, logger, start_winner=None):
        super().__init__(player, logger)
        self.stuck_counter = 0
        self.shoot_counter = 0
        self.jump_counter = 0
        self.current_history = ''
        self.start_winner = start_winner

    @staticmethod
    def detect_repeating_sequence(s):
        pattern = r"(\d{30,})\1{5,}"
        matches = re.findall(pattern, s)
        for match in matches:
            digits = set(match)
            if len(digits) >= 3:
                return True
        return False

    # Use the NN network phenotype and the discrete actuator force function.
    def eval_genome(self, genome, config):
        net = neat.nn.FeedForwardNetwork.create(genome, config)
        fitnesses = []

        for runs in range(1):
            # we reset the environment
            self.player.pyboy.load_state(open('save.state', 'rb'))

            self.current_history = ''
            self.stuck_counter = 0
            self.shoot_counter = 0
            self.jump_counter = 0

            # we initialize the first state of the episode
            next_state = self.get_state_object()

            # wait for the player to spawn
            while not self.player.is_alive():
                self.player.pyboy.tick()

            fitness = 0.0
            done = False
            while not done:
                action = net.activate(next_state)
                next_state, reward, done, selected_action = self.step(action, next_state)
                fitness += reward
                self.current_history += str(selected_action)

                if self.stuck_counter > 50 \
                        or self.shoot_counter > 20 \
                        or self.jump_counter > 20 \
                        or self.detect_repeating_sequence(self.current_history):
                    done = True

            fitnesses.append(fitness)

        return np.mean(fitnesses)

    def eval_genomes(self, genomes, config):
        for genome_id, genome in genomes:
            genome.fitness = self.eval_genome(genome, config)

    def train(self):
        if self.start_winner is not None:
            # Load the winner.
            with open(self.start_winner, 'rb') as f:
                winner = pickle.load(f)

        # Load the config file, which is assumed to live in
        # the same directory as this script.
        config_path = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "config",
                                   "neat_config.ini")
        config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                             neat.DefaultSpeciesSet, neat.DefaultStagnation,
                             config_path)

        pop = neat.Population(config)
        stats = neat.StatisticsReporter()
        pop.add_reporter(stats)
        pop.add_reporter(neat.StdOutReporter(True))

        if self.start_winner is not None:
            self.eval_genome(winner, config)

        else:
            # Run for up to 300 generations.
            winner = pop.run(self.eval_genomes, 300)

            node_names = {
                0: 'Left',
                1: 'Right',
                2: 'Jump',
                3: 'Shoot',
                4: 'Jump Right',
                5: 'Jump Left',
                6: 'Move Up',
                7: 'Move Down',
                8: 'Dash',
            }
            visualize.draw_net(config, winner, True, node_names=node_names)
            visualize.plot_stats(stats, ylog=False, view=True)
            visualize.plot_species(stats, view=True)

            # Save the winner.
            with open('neat-winner.pkl', 'wb') as f:
                pickle.dump(winner, f)

            print(winner)

    def step(self, action, current_state):
        """
        Implementation of the step (Q) function for the Q-Learning model

        Parameters
        ----------
        action : list
            The number of the action to take (0-8)
        current_state : list
            The current state of the episode

        Returns
        -------
        (list, int, bool, int)
            The new state, the reward to give, if the game is over and the selected action
        """
        # We get the current state of the game
        life_before = self.player.life
        x_scroll_before = self.player.scroll.x
        y_scroll_before = self.player.scroll.y

        x_position_before = self.player.position.x
        y_position_before = self.player.position.y
        number_of_enemies_before = len(self.player.get_ennemies())

        # For each action number, make the player do the corresponding action
        max_action = softmax(action)
        action = max_action.index(max(max_action))
        if action == 0:
            self.player.move_left()
        elif action == 1:
            self.player.move_right()
        elif action == 2:
            self.player.jump()
            self.jump_counter += 1
        elif action == 3:
            self.player.shoot()
            self.shoot_counter += 1
        elif action == 4:
            self.player.jump_right()
        elif action == 5:
            self.player.jump_left()
        elif action == 6:
            self.player.move_up()
        elif action == 7:
            self.player.move_down()
        elif action == 6:
            self.player.move_up()
        elif action == 7:
            self.player.move_down()
        elif action == 8:
            self.player.dash()

        if action != 3:
            self.shoot_counter = 0

        if action != 2:
            self.jump_counter = 0

        # Update the player
        self.player.update()

        # Get the next state
        new_state = self.get_state_object()

        # Get the reward
        reward = 0
        done = False

        # If the state is the same as the previous one, we give a negative reward
        if new_state.all() == current_state.all():
            reward -= 30
            self.stuck_counter += 1
        else:
            self.stuck_counter = 0

        # If the player lost life, we give him a negative reward
        if self.player.life < life_before:
            reward -= 5 * abs(self.player.life - life_before)

        # If the player gained life, we give him a positive reward
        # if self.player.life > life_before:
        #    reward += 10 * abs(self.player.life - life_before)

        # If there's a wall in front of the player, give him a negative reward
        if self.player.has_wall_ahead():
            pass  # reward -= 200

        # If there were an enemy before and there's none now, give him a positive reward
        if number_of_enemies_before > 0 and len(self.player.get_ennemies()) == 0:
            reward += 300

        # If the player x scroll is greater than the previous one, give him a reward
        if self.player.scroll.x > x_scroll_before:
            reward += 50 * abs(self.player.scroll.x - x_scroll_before)

        # If the player x scroll is lower than the previous one, give him a negative reward
        if self.player.scroll.x <= x_scroll_before:
            reward = -40 * abs(self.player.scroll.x - x_scroll_before)

        # If the player x position is greater than the previous one, give him a reward
        if self.player.position.x > x_position_before and self.player.scroll.x is x_scroll_before:
            reward += 40 * abs(self.player.position.x - x_position_before)

        # If the player x position is lower than the previous one, give him a negative reward
        if self.player.position.x <= x_position_before and self.player.scroll.x is x_scroll_before:
            reward -= 30 * abs(self.player.position.x - x_position_before)

        '''
        # If the player y scroll is greater than the previous one, give him a reward
        if self.player.scroll.y > y_scroll_before:
            reward += 50 * abs(self.player.scroll.y - y_scroll_before)
    
        # If the player y scroll is lower than the previous one, give him a negative reward
        if self.player.scroll.y <= y_scroll_before:
            reward -= 20 * abs(self.player.scroll.y - y_scroll_before)
    
        # If the player y position is greater than the previous one, give him a reward
        if self.player.position.y > x_position_before and self.player.scroll.y is y_scroll_before:
            reward += 40 * abs(self.player.position.y - y_position_before)
    
        # If the player y position is lower than the previous one, give him a negative reward
        if self.player.position.y <= y_position_before and self.player.scroll.y is y_scroll_before:
            reward -= 15 * abs(self.player.position.y - y_position_before)
        '''

        # If the player is dead, we give him a bigger negative reward
        if not self.player.is_alive():
            reward = -5_000
            done = True

        return new_state, int(reward), done, action

    def get_model_type(self) -> ModelType:
        """
        Returns the model type

        Returns
        -------
        ModelType
            The model type
        """

        return ModelType.NEAT
