from abc import ABC, abstractmethod
from enum import Enum

import numpy as np
from numba import jit
from numpy import ndarray


class ModelType(Enum):
    """
    Enum for the different types of models supported by Megaboy
    """

    Q_LEARNING = 0
    NEAT = 1


class ModelWrapper(ABC):
    """
    Contains the generic methods that can be reused by different model types

    Attributes:
    ----------
    player: Player
        The Player instance to interact with the player on the emulator
    logger: Logger
        The logger instance to use
    unique_ids: dict
        A dictionary containing the unique IDs for a state
    """

    def get_unique_id(self, state):
        """
        Returns the unique ID for a state

        Parameters:
        ----------
        state: Dict
            The state to get the unique ID for

        Returns:
        -------
        int
            The unique ID for the state
        """

        # Convert dict to string
        state_string = str(state)

        # If the state is not in the dictionary as a key, add it and return the new ID
        if state_string not in self.ids:
            value = len(self.ids)
            self.ids[state_string] = value
            return value

        # If the state is in the dictionary, return the ID
        return self.ids[state_string]

    def get_state(self, current_step):
        """
        Returns the current state of the game

        Parameters:
        ----------
        current_step: int
            The current step of the game

        Returns:
        -------
        Dict
            The current state of the game
        """

        return self.get_unique_id({
            'scroll': f"{self.player.scroll.x // 5}",
            'position': f"{self.player.position.x // 5}",
            'ennemies': self.player.get_ennemies(),
            'convoys': 'left' if (self.player.time() // 4) % 2 == 0 else 'right',
        })

    def get_state_object(self) -> ndarray:
        """
        Returns the current state of the game

        Returns:
        -------
        list
            The current state of the game
        """
        tiles = self.player.manager.tilemap_window()
        tiles = tiles[:, :]
        tiles = np.concatenate(tiles, axis=0)
        tiles = np.append(tiles, self.player.scroll.x // 5)
        tiles = np.append(tiles, self.player.position.x // 5)

        return tiles

    @abstractmethod
    def __init__(self, player, logger):
        """
        Initialize the model wrapper

        Parameters:
        ----------
        player: Player
            The Player instance to interact with the player on the emulator
        logger: Logger
            The logger instance to use
        """
        self.player = player
        self.logger = logger
        self.ids = {}
        self.ticks = 0

    @jit(target_backend='cuda', forceobj=True, parallel=True, fastmath=True)
    def train_gpu(self):
        """
        Trains the model using the GPU
        """
        self.train()

    @abstractmethod
    def train(self):
        """
        Trains the model
        """
        raise NotImplementedError

    @abstractmethod
    def get_model_type(self) -> ModelType:
        """
        Get the model type

        Returns:
        -------
        ModelType
            The model type
        """
        raise NotImplementedError
