from abc import ABC

import numpy as np
import keyboard

from src.megaboy.models.Model_Wrapper import ModelWrapper, ModelType
from datetime import datetime


class QLearningModel(ModelWrapper, ABC):
    """
    This is the implementation of the Q-Learning model for the Megaman game

    Attributes
    ----------
    player : Player
        The Player instance to interact with the player on the emulator
    logger : Logger
        The Logger instance to log the events
    number_of_episodes : int
        The number of learning episodes to run
    iteration_count_per_episode : int
        The number of iterations to run per learning episode
    number_of_actions : int
        The number of actions the agent can take (normally 9)
    exploration_probability : float
        The probability of the agent to explore the environment while learning (between 0 and 1)
    q_table : np.array
        The Q-table of the agent
    rewards_history : list
        The list of rewards received by the agent
    """

    # Exploration probability, can't go below this value, or it'll be stuck
    MIN_EXPLORATION_PROBABILITY = .01

    # Discounted factor, keep it to 0.99 for now until we find better
    GAMMA = .99

    # Learning rate, keep it to 0.1 for now, should be 0.1 for best Q-learning we can get
    LEARNING_RATE = .1

    # Exploration decreasing decay for exponential decreasing. Useless for now, keep it to .001 at beginning
    EXPLORATION_DECREASING_DECAY = .01

    # Q-Learning array size
    Q_TABLE_SIZE = 100_000_000

    def __init__(self, player, logger, start_table=None):
        """
        Initialize the Q-Learning model

        Parameters
        ----------
        player : Player
            The Player instance to interact with the player on the emulator
        logger : Logger
            The Logger instance to log the events
        start_table : NumPy array
            Start the Q-table with a pre-existing table from a previous training
        """

        super().__init__(player, logger)
        self.number_of_episodes = self.Q_TABLE_SIZE
        self.iteration_count_per_episode = self.Q_TABLE_SIZE

        self.number_of_actions = 10
        self.exploration_probability = .6
        self.rewards_history = []

        if start_table is None:
            self.q_table = np.zeros((self.Q_TABLE_SIZE, self.number_of_actions))
        else:
            self.q_table = start_table

        # When S key is pressed, the current Q-table is saved
        keyboard.on_press_key("s", lambda _: self.save_model())

    def save_model(self):
        """
        Save the current Q-table to a file in the results folder
        """

        # Q-table
        filename = f"../results/q-learning/q_table_{datetime.now().strftime('%Y_%m_%d-%H_%M_%S')}.npy"
        self.logger.info(f"Saving model to {filename}")
        np.save(filename, self.q_table)

        # Rewards history
        filename = f"../results/q-learning/rewards_history_{datetime.now().strftime('%Y_%m_%d-%H_%M_%S')}.npy"
        self.logger.info(f"Saving rewards history to {filename}")
        np.save(filename, self.rewards_history)

    def train(self):
        """
        Train the Q-Learning model

        Notice: This is a very long process, it can take a few hours to run depending on your computer
                If your device has a GPU, it will be faster
        """

        total_rewards_episode = list()
        self.player.pyboy.save_state(open('save.state', 'wb'))

        # we iterate over episodes
        for e in range(self.number_of_episodes):
            # we reset the environment
            self.player.pyboy.load_state(open('save.state', 'rb'))

            # we initialize the first state of the episode
            current_step = 0
            current_state = self.get_state(current_step)

            # wait for the player to spawn
            while not self.player.is_alive():
                self.player.pyboy.tick()
            self.player.start_time = datetime.now()

            # sum the rewards that the agent gets from the environment
            total_episode_reward = 0
            start_time = datetime.now()

            for i in range(self.iteration_count_per_episode):
                # we sample a float from a uniform distribution over 0 and 1
                # if the sampled float is less than the exploration proba
                #     the agent selects a random action
                # else
                #     he exploits his knowledge using the bellman equation
                if np.random.uniform(0, 1) < self.exploration_probability:
                    action = np.random.randint(0, self.number_of_actions)
                else:
                    action = np.argmax(self.q_table[current_state, :])

                # The environment runs the chosen action and returns
                # the next state, a reward and true if the episode is ended.
                next_state, reward, done, current_step = self.step(action, current_step, current_state)

                # We update our Q-table using the Q-learning iteration
                # see Q-learning algorithm formula
                self.q_table[current_state, action] = (1 - self.LEARNING_RATE) * self.q_table[
                    current_state, action] + self.LEARNING_RATE * (reward + self.GAMMA * max(
                    self.q_table[next_state, :]))
                total_episode_reward = total_episode_reward + reward

                # If the episode is finished, we leave the for loop
                if done:
                    break
                current_state = next_state

            end_time = datetime.now()

            # We update the exploration proba using exponential decay formula
            self.exploration_probability = max(self.MIN_EXPLORATION_PROBABILITY,
                                               np.exp(-self.EXPLORATION_DECREASING_DECAY * e))
            total_rewards_episode.append(total_episode_reward)
            self.rewards_history.append({
                "episode": e,
                "reward": total_episode_reward,
                "exploration_probability": self.exploration_probability,
                "time": end_time - start_time,
            })
            self.logger.info(f"Episode {e} finished with reward {total_episode_reward}")

        # We save the Q-table
        np.save(f"../results/q-learning/q_table_{datetime.now().strftime('%Y_%m_%d-%H_%M_%S')}.npy", self.q_table)

    def step(self, action, current_step, old_state):
        """
        Implementation of the step (Q) function for the Q-Learning model

        Parameters
        ----------
        action : int
            The number of the action to take (0-8)
        current_step : int
            The current step of the episode
        old_state : int
            The old state of the episode

        Returns
        -------
        (int, int, bool, int)
            The next state, the reward to give, if the game is over and new step of the episode
        """

        # We get the current state of the game
        life_before = self.player.life
        x_scroll_before = self.player.scroll.x
        y_scroll_before = self.player.scroll.y

        x_position_before = self.player.position.x
        y_position_before = self.player.position.y
        number_of_enemies_before = len(self.player.get_ennemies())

        # For each action number, make the player do the corresponding action
        if action == 0:
            self.player.move_left()
        elif action == 1:
            self.player.move_right()
        elif action == 2:
            self.player.jump()
        elif action == 3:
            self.player.shoot()
        elif action == 4:
            self.player.jump_right()
        elif action == 5:
            self.player.jump_left()
        elif action == 6:
            self.player.move_up()
        elif action == 7:
            self.player.move_down()
        elif action == 6:
            self.player.move_up()
        elif action == 7:
            self.player.move_down()
        elif action == 8:
            self.player.dash()

        # Update the player
        self.player.update()

        # Get the next state
        new_state = self.get_state(current_step)

        # Get the reward
        reward = 0
        done = False

        # If the state is the same as the previous one, we give a negative reward
        if new_state == old_state:
            reward -= 30

        # If the player lost life, we give him a negative reward
        if self.player.life < life_before:
            reward -= 5 * abs(self.player.life - life_before)

        # If the player gained life, we give him a positive reward
        # if self.player.life > life_before:
        #    reward += 10 * abs(self.player.life - life_before)

        # If there's a wall in front of the player, give him a negative reward
        if self.player.has_wall_ahead():
            pass  # reward -= 200

        # If there were an enemy before and there's none now, give him a positive reward
        if number_of_enemies_before > 0 and len(self.player.get_ennemies()) == 0:
            reward += 300

        # If the player x scroll is greater than the previous one, give him a reward
        if self.player.scroll.x > x_scroll_before:
            reward += 50 * abs(self.player.scroll.x - x_scroll_before)

        # If the player x scroll is lower than the previous one, give him a negative reward
        if self.player.scroll.x <= x_scroll_before:
            reward = -20 * abs(self.player.scroll.x - x_scroll_before)

        # If the player x position is greater than the previous one, give him a reward
        if self.player.position.x > x_position_before and self.player.scroll.x is x_scroll_before:
            reward += 40 * abs(self.player.position.x - x_position_before)

        # If the player x position is lower than the previous one, give him a negative reward
        if self.player.position.x <= x_position_before and self.player.scroll.x is x_scroll_before:
            reward -= 15 * abs(self.player.position.x - x_position_before)

        '''
        # If the player y scroll is greater than the previous one, give him a reward
        if self.player.scroll.y > y_scroll_before:
            reward += 50 * abs(self.player.scroll.y - y_scroll_before)

        # If the player y scroll is lower than the previous one, give him a negative reward
        if self.player.scroll.y <= y_scroll_before:
            reward -= 20 * abs(self.player.scroll.y - y_scroll_before)

        # If the player y position is greater than the previous one, give him a reward
        if self.player.position.y > x_position_before and self.player.scroll.y is y_scroll_before:
            reward += 40 * abs(self.player.position.y - y_position_before)

        # If the player y position is lower than the previous one, give him a negative reward
        if self.player.position.y <= y_position_before and self.player.scroll.y is y_scroll_before:
            reward -= 15 * abs(self.player.position.y - y_position_before)
        '''

        # If the player is dead, we give him a bigger negative reward
        if not self.player.is_alive():
            reward = -1_000
            done = True

        # If the player position and scroll have changed, increase the step
        if self.player.position.x != x_position_before or self.player.scroll.x != x_scroll_before:
            current_step += 1

        return new_state, int(reward), done, current_step

    def get_model_type(self):
        return ModelType.Q_LEARNING
