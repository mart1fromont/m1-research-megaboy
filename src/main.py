import os

from src.megaboy.models.Model_Wrapper import ModelType
from src.utils.Logger import Logger
from src.megaboy.Emulator import Emulator

if __name__ == '__main__':
    """
    Run the main program
    Launches an Emulator of Megaman
    """

    # Initialize a new logger
    logger = Logger()
    logger.info('Logger initialized')

    # Get the save state to start directly from the first level
    save_state = os.path.join(os.path.dirname(os.path.dirname(__file__)), "tools", "megaman.gb.state")
    save_state_file = open(save_state, "rb")

    # Launch the emulator
    # start_winner='neat-winner.pkl'
    emulator = Emulator(load_state=save_state_file, logger=logger, model=ModelType.Q_LEARNING)
