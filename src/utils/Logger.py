import logging
import datetime
import os


class Formatter(logging.Formatter):
    """
    Logger formatter class
    This is the console formatter used by the logger

    Extends:
        logging.Formatter
    """

    grey = '\x1b[38;5;5m'
    blue = '\x1b[38;5;39m'
    yellow = '\x1b[38;5;226m'
    red = '\x1b[38;5;196m'
    bold_red = '\x1b[31;1m'
    reset = '\x1b[0m'

    def __init__(self, console=True):
        """
        Inits a new formatter

        Parameters:
        ----------
        console : bool
            If True, the formatter will be used for a console use. If False, the formatter will be used for a file use
        """

        super().__init__()
        self.fmt = '%(levelname)8s | %(asctime)s | %(message)s'
        if console:
            self.FORMATS = {
                logging.DEBUG: self.grey + self.fmt + self.reset,
                logging.INFO: self.blue + self.fmt + self.reset,
                logging.WARNING: self.yellow + self.fmt + self.reset,
                logging.ERROR: self.red + self.fmt + self.reset,
                logging.CRITICAL: self.bold_red + self.fmt + self.reset
            }
        else:
            self.FORMATS = {
                logging.DEBUG: self.fmt,
                logging.INFO: self.fmt,
                logging.WARNING: self.fmt,
                logging.ERROR: self.fmt,
                logging.CRITICAL: self.fmt
            }

    def format(self, message):
        """
        Formats a log message

        Parameters:
        ----------
        message : logging.LogRecord
            The log message to format

        Returns:
        -------
        str
            The formatted log message
        """

        return logging.Formatter(self.FORMATS.get(message.levelno)).format(message)


class Logger:
    """
    Logger class
    """

    def __init__(self):
        """
        Inits and configure a logger instance
        """

        # If the logs directory does not exist, create it
        if not os.path.isdir(os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "logs")):
            os.mkdir(os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "logs"))

        # Create custom logger utils all five levels
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)

        # Create stdout handler for utils to the console (logs all five levels)
        stdout_handler = logging.StreamHandler()
        stdout_handler.setLevel(logging.DEBUG)
        stdout_handler.setFormatter(Formatter())

        # Create file handler for utils to a file (logs all five levels)
        file_handler = logging.FileHandler('../logs/megaboy_{}.log'.format(datetime.datetime.now().strftime(
                                                                                                '%Y_%m_%d-%H_%M_%S')))
        file_handler.setLevel(logging.DEBUG)
        file_handler.setFormatter(Formatter(False))

        # Add both handlers to the logger
        self.logger.addHandler(stdout_handler)
        self.logger.addHandler(file_handler)

    def debug(self, message):
        """
        Logs a debug message

        Parameters:
        ----------
        message : str
            The message to log
        """

        self.logger.debug(message)

    def info(self, message):
        """
        Logs an info message

        Parameters:
        ----------
        message : str
            The message to log
        """

        self.logger.info(message)

    def warning(self, message):
        """
        Logs a warning message

        Parameters:
        ----------
        message : str
            The message to log
        """

        self.logger.warning(message)

    def error(self, message):
        """
        Logs an error message

        Parameters:
        ----------
        message : str
            The message to log
        """

        self.logger.error(message)

    def critical(self, message):
        """
        Logs a critical message

        Parameters:
        ----------
        message : str
            The message to log
        """

        self.logger.critical(message)
