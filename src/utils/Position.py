class Position:
    """
    Position class
    Store an (x, y) position on a 2D tile-map

    Attributes:
    ----------
    x: int
        The x position
    y: int
        The y position
    """

    def __init__(self, x, y):
        """
        Constructor
        """

        self.x = x
        self.y = y

    def __str__(self) -> str:
        """
        Classic __str__

        Returns:
        -------
        str
            The string representation of the position
        """

        return '({}, {})'.format(self.x, self.y)
