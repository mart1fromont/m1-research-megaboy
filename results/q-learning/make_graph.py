import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

FILENAME = 'rewards_history_2023_06_11-19_58_36.npy'

if __name__ == '__main__':
    # read the npy file into a dataframe
    df = pd.DataFrame(np.load(FILENAME, allow_pickle=True))
    probabilities = []
    rewards = []

    for data in df[0]:
        probabilities.append(data['exploration_probability'])
        rewards.append(data['reward'])

    # create the rewards graph
    plt.plot(rewards, label='rewards', color='blue', linewidth=0.5, linestyle='-', marker='o', markersize=2)
    plt.xlabel('episode')
    plt.ylabel('reward')
    plt.title('Rewards')

    # draw a line to show the evolution of the average reward
    avg_rewards = []
    for i in range(len(rewards)):
        avg_rewards.append(np.mean(rewards[:i+1]))
    plt.plot(avg_rewards, label='average reward', color='green', linewidth=1, linestyle='-', marker='o', markersize=2)


    # show the legend
    plt.legend()

    # save the graph
    plt.savefig('rewards.png')

    # create the exploration probability graph
    plt.clf()
    plt.semilogy(probabilities, label='exploration probability', color='red', linewidth=1, linestyle='-', marker='o', markersize=2)
    plt.xlabel('episode')
    plt.ylabel('exploration probability')
    plt.title('Exploration Probability')

    # show the legend
    plt.legend()

    # save the graph
    plt.savefig('exploration_probability.png')
