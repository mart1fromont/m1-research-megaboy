# Megaboy: a PyBoy IA for Megaman 5

![GitHub](https://img.shields.io/badge/from-Master's%201%20research%20project-important)
![made-with-python](https://img.shields.io/badge/made%20with-Python-informational)
![GitLab tag (latest by date)](https://img.shields.io/gitlab/v/tag/mart1fromont/m1-research-megaboy?label=latest)

## Authors:
![GitLab contributors](https://img.shields.io/gitlab/contributors/mart1fromont/m1-research-megaboy)
- [Baptiste Duquenne](https://gitlab.com/bapt.duquenne)
- [Martin Fromont](https://gitlab.com/mart1fromont)

## Getting started

### Prerequisites

- Python 3.10
- Install necessary dependencies with `pip install -r requirements.txt`
- See [PyBoy installation](https://img.shields.io/badge/made%20with-Python-informational) if your system isn't 
supported by [pysdl2-dll](https://pypi.org/project/pysdl2-dll/) (required to run PyBoy)

### Running

Simply launch `main.py` file from a python interpreter. By default, it will launch a save state of Megaman 5 and start
playing it.

### Emulator

Megaboy uses a Python class named `Emulator` to interact with the PyBoy and train the different IAs.
You can configure the behaviour of Megaboy by changing the parameters of the `Emulator` class:

- `rom_path_file`: path to the Megaman V ROM file. By default, it will use the save state provided in the `tools/` folder
- `load_state`: path to the save state file. By default, it will also use the save state provided in the `tools/` folder
- `logger`: logger to use. Should be an instance of `Logger` class. If not provided, it will use a new instance of `Logger`
- `model`: IA model to use. It should be a `ModelType` enum. If not provided, it will use `ModelType.Q_LEARNING`
- `start_q_table`: can only be used when `model` is `ModelType.Q_LEARNING`. Path to an existing Q-table file from a previous training. If not provided, it will start from a new Q-table

## Description

This project is the result of our researches on a PyBoy IA for Megaman 5. It's based on the 
[PyBoy](https://github.com/Baekalfen/PyBoy) emulator for Python 3.x and designed to run with the original GameBoy 
Megaman 5's ROM (which can be found in the `tools/` folder).
